package fr.eservices.drive.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
@Component
@Qualifier("MD5")
public class MD5Checker implements PasswordChecker {

	@Override
	public String encode(String login, String password) {
        MessageDigest md;
        byte[] md5hash = null;
        byte[] concatBytes;
        String concat = login + password;
        
        try {
			md = MessageDigest.getInstance("MD5");
			concatBytes = concat.getBytes();
			md.update(concatBytes);
			md5hash = md.digest();
			return Base64.getEncoder().encodeToString(md5hash);
        } catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
        
		return null;
	}
	
}
