package fr.eservices.drive.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
@Component
@Qualifier("HmacSHA1")

public class HmacChecker implements PasswordChecker {
	
	@Override
	public String encode(String login, String password) {
		byte[] passwordBytes = password.getBytes();
		try {
			Mac mac = Mac.getInstance("HmacSHA1");
			SecretKeySpec signingKey=new SecretKeySpec(login.getBytes(),"HmacSHA1");
			mac.init(signingKey);
			byte[] hashBytes = mac.doFinal(passwordBytes);
			return Base64.getEncoder().encodeToString(hashBytes);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
