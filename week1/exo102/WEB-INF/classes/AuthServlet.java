import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;  


@WebServlet("/auth")
public class AuthServlet extends HttpServlet {

	// TODO : only handle POST request for authentication
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		// TODO : get login / password from request parameters
		String login = req.getParameter("login");
		String password = req.getParameter("password");
		Writer out = resp.getWriter();
		out.write("login : "+ login);
		out.write("pass: "+ password);
		
		if ( login == null || password == null ) throw new ServletException("no login/password");
		boolean succeed = "admin".equals(login) && "admin".equals(password);

		if(succeed){
			// add something in session for next calls,
			HttpSession session=req.getSession();
	        if(session !=null) {
	    	    session.setAttribute("login",login);
	    	    session.setAttribute("loginSession",session.getId());
	    	    
			}
			// then redirect to "welcome.jsp"
			resp.sendRedirect("welcome.jsp");
		}else{
         // set an "errorMessage" in request attribute
         req.setAttribute("errorMessage", "Invalid user or password");
		 // forward to auth.jsp with request dispatcher
		 req.getRequestDispatcher("auth.jsp").forward(req, resp);
		}

	}
	
	// TODO : allow to disconnect with a GET to /auth with any parameter "logout" value
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
				PrintWriter out = resp.getWriter();

		// check for "logout" parameter
		if(req.getParameter("logout") != null){
			//   if so : disconnect and show auth.jsp
			req.getSession().invalidate();  
         	req.getRequestDispatcher("auth.jsp").forward(req, resp);   
		}
		else {
			//   if not : Error 500
			resp.sendError(500);
		}
	}

}