<%
// TODO : check if authenticated !
//   if not : show an error an a link to auth.jsp
// Not logged in, redirect to login page.
HttpSession session1 = request.getSession();
if (session1 == null || session1.getAttribute("login") == null || !session1.getId().equals(session1.getAttribute("loginSession"))) {
	request.setAttribute("errorMessage", "Login required");
    request.getRequestDispatcher("auth.jsp").forward(request, response);
}
%>
<h1>Welcome Admin !</h1>

[ <a href="auth?logout" style="color: red; text-decoration: none">DISCONNECT</a> ]
