import java.io.IOException;
import java.io.Writer;
import static java.lang.System.*;
import java.text.DecimalFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/dist")
public class MyServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		resp.setContentType("text/html");
		Writer out = resp.getWriter();

		out.write("<html><body>");
		out.write("<h1>formulaire de distance</h1>");
		out.write("<form ACTION="+"'./dist'"+" METHOD=POST>");
		out.write("<label for="+"'p1lat'"+">p1lat :</label>");
		out.write("<input type="+"'text'"+" name="+"'p1lat'" +"/><br>");
		out.write("<label for="+"'p1lng'"+">p1lng :</label>");
		out.write("<input type="+"'text'"+" name="+"'p1lng'" +"/><br>");
		out.write("<label for="+"'p2lat'"+"> p2lat :</label>");
		out.write("<input type="+"'text'"+" name="+"'p2lat'" +"/><br>");
		out.write("<label for="+"'p2lng'"+"> p2lng:</label>");
		out.write("<input type="+"'text'"+"name="+"'p2lng'"+"/><br>");
		out.write("<input type=submit value="+"'calculer'"+">");
		out.write("</form></body></html>");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
	throws ServletException, IOException {
		
		double p1lat  = Double.parseDouble(req.getParameter("p1lat"));

		double p1lng  = Double.parseDouble(req.getParameter("p1lng"));

		double p2lat  = Double.parseDouble(req.getParameter("p2lat"));

		double p2lng  = Double.parseDouble(req.getParameter("p2lng"));
		
		double R = 6371;
		double φ1 = Math.toRadians(p1lat);
		double φ2 = Math.toRadians(p2lat);
		double Δφ = Math.toRadians(p2lat-p1lat);
		double Δλ = Math.toRadians(p2lng-p1lng);

		double a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
			Math.cos(φ1) * Math.cos(φ2) *
			Math.sin(Δλ/2) * Math.sin(Δλ/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

		double d = R * c;
     		DecimalFormat f = new DecimalFormat("##.0");


		resp.setContentType("text/html");
		Writer out = resp.getWriter();
		out.write("<html><body>");
		out.write("<h1>Calcul de la distance</h1>");
		out.write("la distance entre les deux points est :"+f.format(d) +" km");
		out.write("</body></html>");

	}

}
