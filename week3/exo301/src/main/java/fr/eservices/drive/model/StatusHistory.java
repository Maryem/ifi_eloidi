package fr.eservices.drive.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class StatusHistory {
	@Id
	@GeneratedValue
	private long id;
	@Temporal(TemporalType.DATE)
	@Column(name="STATUS_DATE")
	private Date statusDate;
	private Status status;
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public StatusHistory(long id, Date statusDate) {
		this.id = id;
		this.statusDate = statusDate;
	}

	public long getId() {
		return this.id;
	}

	public Date getStatusDate() {
		return this.statusDate;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setStatusDate(Date s) {
		this.statusDate = s;
	}
}
