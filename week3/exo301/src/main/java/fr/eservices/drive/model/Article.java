package fr.eservices.drive.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name="Article")
public class Article implements Serializable{
	@Id
	@GeneratedValue
    @Column(name = "id")

	private long id;
	private String ean;
	private Double price;
	private Double vat;
	private String name;
	private File img;
	@ManyToMany
	List<Category> categories = new ArrayList<>();
	public Article(long id,String ean13,Double price,Double vat,String name,File img){
		this.id = id;
		this.ean = ean;
		this.price = price;
		this.vat = vat;
		this.name = name;
		this.img = img;
	}
	public Article() {
		
	}
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	public long getId(){
		return this.id;
	}
	public String getEan13(){
		return this.ean;
	}
	public Double getPrice(){
		return this.price;
	}
	public Double getVat(){
		return this.vat;
	}
	public String getName(){
		return this.name;
	}
	public File getImg(){
		return this.img;
	}
	public void setId(long id){
		this.id = id;
	}
	public void setEan13(String ean){
		this.ean = ean;
	}
	public void setPrice(Double p){
		this.price = p;
	}
	public void setVat(Double vat){
		this.vat = vat;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setImg(File img){
		this.img = img;
	}
}