package fr.eservices.drive.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)

public class Perishable extends Article {
	@Temporal(TemporalType.DATE)
	@Column(name = "BEST_BEFORE")
	private Date bestBefore;
	private String lot;

	public Perishable(Date b, String l) {
		super();
		this.bestBefore = b;
		this.lot = l;
	}

	public Date getBestBefore() {
		return this.bestBefore;
	}

	public String getLot() {
		return this.lot;
	}

	public void setBestBefore(Date b) {
		this.bestBefore = b;
	}

	public void setLot(String l) {
		this.lot = l;
	}
}
