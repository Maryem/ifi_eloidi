package fr.eservices.drive.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="Category")
public class Category implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="id")
	private long id;
	
	private String name;
	
	private int orderIdx;

	public Category() {}
	
	public Category(long id, String name, int orderIdx) {
		this.id = id;
		this.name = name;
		this.orderIdx = orderIdx;
	}

	public long getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public int getOrderIdx() {
		return this.orderIdx;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOrderIdx(int orderIdx) {
		this.orderIdx = orderIdx;
	}
}
