package fr.eservices.drive.model;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Order {
	@Id
	@GeneratedValue
	private long id;
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_ON")

	private Date createdOn;
	@Temporal(TemporalType.DATE)
	@Column(name="DELIVERY_DATE")

	private Date deliveryDate;
	private int amount;
	@ManyToMany
	private List<Article> articles = new ArrayList<>();

	@ManyToOne
	private Customer customer;
	@OneToMany
	private List<StatusHistory> history = new ArrayList<>();

	private Status currentStatus;

	public Status getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(Status currentStatus) {
		this.currentStatus = currentStatus;
	}

	

	public List<StatusHistory> getHistory() {
		return history;
	}

	public void setHistory(List<StatusHistory> history) {
		this.history = history;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public Order(long id, Date c, Date d, int a) {
		this.id = id;
		this.createdOn = c;
		this.deliveryDate = d;
		this.amount = a;
	}

	public long getId() {
		return this.id;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public int getAmount() {
		return this.amount;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setCreatedOn(Date c) {
		this.createdOn = c;
	}

	public void setDeliveryDate(Date d) {
		this.deliveryDate = d;
	}

	public void setAmount(int a) {
		this.amount = a;
	}
}
