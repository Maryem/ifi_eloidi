package fr.eservices.drive.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import fr.eservices.drive.model.Article;
import fr.eservices.drive.model.Category;
import fr.eservices.drive.model.Perishable;

public class CatalogDaoJPAImpl implements CatalogDao {
    @PersistenceContext
	EntityManager em;
	
	public CatalogDaoJPAImpl(EntityManager em) {
		 this.em = em;
	}

	@Override
	public List<Category> getCategories() throws DataException{		
		List<Category> cats = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Category> cq = cb.createQuery(Category.class);
		Root<Category> c = cq.from(Category.class);
		cq.select(c).distinct(true).orderBy(cb.asc(c.get("orderIdx")));

		cats = em.createQuery(cq).getResultList();
		return cats;
	}

	@Override
	public List<Category> getArticleCategories(int id) throws DataException{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Category> cq = cb.createQuery(Category.class);
		Root<Article> article = cq.from(Article.class);		
		cq.select(article.get("categories")).where(cb.equal(article.get("id"), id));
		return em.createQuery(cq).getResultList();
		
	}

	@Override
	public List<Article> getCategoryContent(int categoryId) throws DataException{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Article> cq = cb.createQuery(Article.class);
		Root<Article> art = cq.from(Article.class);
		Join<Article,Category> category = art.join("categories");
		cq.where(cb.equal(category.get("id"), categoryId));
		
		return em.createQuery(cq).getResultList();
		
	}

	@Override
	public List<Perishable> getPerished(Date day) throws DataException{
		return new ArrayList<Perishable>();
	}

	
}
