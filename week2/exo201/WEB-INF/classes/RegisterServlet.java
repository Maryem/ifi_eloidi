

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;

import user.UserDao;
import user.User;
import user.UserDaoSqlite;

@WebServlet(urlPatterns="/register")
public class RegisterServlet extends HttpServlet {
	
	private UserDao userDao;
	
	@Override
	public void init() throws ServletException {
		try{
			// TODO : initialize user DAO
			this.userDao = new UserDaoSqlite("../users.db");
		}catch (SQLException e) {
		    System.out.println(e.getMessage());
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		// TODO : create user using dao and redirect to login page
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		String confirmPassword = req.getParameter("confirmPassword");

		String missingFields = "";

		if(firstname == null || firstname.isEmpty()){
			if(!missingFields.isEmpty()) {
				missingFields += ", ";
			}
			missingFields += "firstname";
			req.setAttribute("firstnameError", "firstname field is missing");
		}

		if(lastname == null || lastname.isEmpty()){
			if(!missingFields.isEmpty()) {
				missingFields += ", ";
			}
			missingFields += "lastname";
			req.setAttribute("lastnameError", "lastname field is missing");
		}

		if(email == null || email.isEmpty()){
			if(!missingFields.isEmpty()) {
				missingFields += ", ";
			}
			missingFields += "email";
			req.setAttribute("emailError", "email field is missing");
		}

		if(password == null || password.isEmpty()){
			if(!missingFields.isEmpty()) {
				missingFields += ", ";
			}
			missingFields += "password";
			req.setAttribute("passwordError", "password field is missing");
		}

		if(missingFields.isEmpty()) {
			if(this.userDao.exists(email) == -1){
				if(password.equals(confirmPassword)){
					User user = new User();
					user.setFirstname(firstname);
					user.setLastname(firstname);
					user.setEmail(firstname);
					this.userDao.add(user, password);
					resp.setStatus(302);
					resp.setHeader("Location", "auth.jsp");
					//req.getRequestDispatcher("auth.jsp").forward(req, resp);
					
				}
			
				else{
			         req.setAttribute("errorMessage", "Password and password confirm are not the same");
			         req.setAttribute("passwordError", "Passwords are not the same");
			         req.setAttribute("passwordConfirmError", "Passwords are not the same");
					 req.getRequestDispatcher("register.jsp").forward(req, resp);
				}
			}
			else{
		         req.setAttribute("errorMessage", "Email already exists");
		         req.setAttribute("emailError", "Email already exists");
				 req.getRequestDispatcher("register.jsp").forward(req, resp);
			}
		}
		else {
			if(missingFields.contains(",")) {
				req.setAttribute("errorMessage", missingFields + " fields are missing.");
			}
			else {
				req.setAttribute("errorMessage", missingFields + " field is missing");
			}

		 	req.getRequestDispatcher("register.jsp").forward(req, resp);
		}
	}
}
