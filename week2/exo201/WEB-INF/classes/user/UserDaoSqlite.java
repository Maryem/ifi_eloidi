package user;

import java.sql.*;

public class UserDaoSqlite implements UserDao {
	
	static {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			throw new Error(e);
		}
	}
	
	protected Connection conn;
	public UserDaoSqlite( String userFilePath ) throws SQLException {
		
      // db parameters
			String jdbcUrl = "jdbc:sqlite:"+userFilePath;
      // create a connection to the database
      this.conn = DriverManager.getConnection(jdbcUrl);
      
      System.out.println("Connection to SQLite has been established.");


	}
	
	@Override
	public void add(User user, String password) {
		// TODO : create a user
        String sql = "INSERT INTO users(firstname,lastname,email,password) VALUES(?,?,?,?)";
 
        try {
           	PreparedStatement pstmt = this.conn.prepareStatement(sql);
            pstmt.setString(1, user.getFirstname());
            pstmt.setString(2, user.getLastname());
            pstmt.setString(3, user.getEmail());
            pstmt.setString(4, password);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    

	}
	
	@Override
	public void update(User user, String password) {
		// TODO : update user information in DB
		String sql = "UPDATE users SET firstname = ?, lastname = ?, email = ?, password = ? WHERE id = ?";
		try {
           	PreparedStatement pstmt = this.conn.prepareStatement(sql) ;
            pstmt.setString(1, user.getFirstname());
            pstmt.setString(2, user.getLastname());
            pstmt.setString(3, user.getEmail());
            pstmt.setString(4, password);
            pstmt.setLong(5,user.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}
	
	@Override
	public User find(long id) {
		// TODO : get user data by its ID and map to User object
		String sql = "SELECT * from users WHERE id ="+id;
		try( 
           	Statement stmt = this.conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql)){
           
           long ID = rs.getLong("id");
           String firstname= rs.getString("firstname");
           String lastname= rs.getString("lastname");
           String email= rs.getString("email");

           User user = new User();
           user.setId(ID);
           user.setFirstname(firstname);
           user.setLastname(lastname);
           user.setEmail(email);

           return user;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		return null;
	}
	
	@Override
	public User findByEmail(String email) {
		// TODO : get user data by its ID and map to User object
		String sql = "SELECT * from users WHERE email =?";
		try{ 
            PreparedStatement pstmt = this.conn.prepareStatement(sql) ;
            pstmt.setString(1, email);

            ResultSet rs    = pstmt.executeQuery();
            if(rs!=null){
           long id = rs.getLong("id");
           String firstname= rs.getString("firstname");
           String lastname= rs.getString("lastname");
           String mail= rs.getString("email");
           
           User user = new User();
           user.setId(id);
           user.setFirstname(firstname);
           user.setLastname(lastname);
           user.setEmail(mail);

           return user;
         }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		 return null;
	}
	
	@Override
	public long checkPassword(String email, String password) {
		// TODO : get user id that match, return -1 if none
		String sql = "SELECT id from users WHERE email = ? and password = ?";

    try{
      PreparedStatement pstmt = this.conn.prepareStatement(sql) ;
      pstmt.setString(1, email);
      pstmt.setString(2, password);

      ResultSet rs    = pstmt.executeQuery();

			if(rs!=null) {           
        long id = rs.getLong("id");
        
        return id;
        
       }
       	
    } catch (SQLException e) {
        System.out.println(e.getMessage());
    }
    return -1;
	}
	
	@Override
	public void delete(long id) {
		// TODO : delete a user that match this ID
      String sql = "DELETE FROM users WHERE id = ?";
      try(PreparedStatement pstmt = this.conn.prepareStatement(sql)) {
      	 pstmt.setLong(1, id);
         pstmt.executeUpdate();

      }catch (SQLException e) {
        System.out.println(e.getMessage());
      }
	}
	
	@Override
	public long exists(String email) {
		// TODO : check if user with that mail exists
		String sql = "SELECT * from users WHERE email ="+email;
		try( 
     	Statement stmt = this.conn.createStatement();
      ResultSet rs    = stmt.executeQuery(sql)){
	
  		if(rs!=null){
       	return rs.getLong("id");
       }
    } catch (SQLException e) {
        System.out.println(e.getMessage());
    }

    return -1;
	}
	
	

}